package io.space.xrateskit.entities

data class MarketInfoKey(
        val coin: String,
        val currency: String
)

