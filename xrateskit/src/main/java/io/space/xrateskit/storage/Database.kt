package io.space.xrateskit.storage

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.space.xrateskit.entities.ChartPointEntity
import io.space.xrateskit.entities.HistoricalRate
import io.space.xrateskit.entities.MarketInfoEntity

@androidx.room.Database(version = 2, exportSchema = false, entities = [
        HistoricalRate::class,
        ChartPointEntity::class,
        MarketInfoEntity::class
    ])

@TypeConverters(DatabaseConverters::class)

abstract class Database : RoomDatabase() {
    abstract val historicalRateDao: HistoricalRateDao
    abstract val chartPointDao: ChartStatsDao
    abstract val marketInfoDao: MarketInfoDao

    companion object {
        fun create(context: Context): Database {
            return Room.databaseBuilder(context, Database::class.java, "x-rate-database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}
