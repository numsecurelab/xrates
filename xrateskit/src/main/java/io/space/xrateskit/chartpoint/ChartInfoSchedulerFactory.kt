package io.space.xrateskit.chartpoint

import io.space.xrateskit.core.IChartInfoProvider
import io.space.xrateskit.entities.ChartInfoKey

class ChartInfoSchedulerFactory(
        private val manager: ChartInfoManager,
        private val provider: IChartInfoProvider,
        private val retryInterval: Long) {

    fun getScheduler(key: ChartInfoKey): ChartInfoScheduler {
        return ChartInfoScheduler(ChartInfoSchedulerProvider(retryInterval, key, provider, manager))
    }
}
